import os
from flask import Flask, render_template, request

app = Flask(__name__)
#app.config.from_object(os.environ['APP_SETTINGS'])

@app.route('/givewish')
def give_wish():
	return render_template('entry.html', 
		the_title = 'give a wish')

@app.route('/wishlist',methods = ['GET','POST'])
def wishlist():
	wish = request.form['wish']
	return render_template('results.html',
		the_wish = wish,
		the_title = 'your wish')

if __name__ == '__main__':
	app.run()